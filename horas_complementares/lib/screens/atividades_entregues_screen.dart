import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:horas_complementares/model/user_model.dart';
import 'package:horas_complementares/screens/login_screen.dart';
import 'package:horas_complementares/datas/atividade_data.dart';
import 'package:horas_complementares/tiles/atividade_tile.dart';


class AtividadeEntregueScreen extends StatelessWidget {

  final DocumentSnapshot snapshot;

  AtividadeEntregueScreen(this.snapshot);

  @override
  Widget build(BuildContext context) {

    if(UserModel.of(context).isLoggedIn()) {
      String uid = UserModel
          .of(context)
          .firebaseUser
          .uid;

      return DefaultTabController(
        length: 2,
        child: Scaffold(
            appBar: AppBar(
              title: Text("Atividades Entregues"),
              centerTitle: true,
              bottom: TabBar(
                indicatorColor: Colors.white,
                tabs: <Widget>[
                  Tab(icon: Icon(Icons.grid_on),),
                  Tab(icon: Icon(Icons.list),)
                ],
              ),
            ),
            body: FutureBuilder<QuerySnapshot>(
                future: Firestore.instance
                    .collection("users")
                    .document(uid)
                    .collection("atividades")
                    .document(snapshot.documentID)
                    .collection("10") // precisa de uma forma de acessar todos os meses, talvez um for?
                    .getDocuments(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData)
                    return Center(child: CircularProgressIndicator(),);
                  else
                    return TabBarView(
                        physics: NeverScrollableScrollPhysics(),
                        children: [
                          GridView.builder(
                              padding: EdgeInsets.all(4.0),
                              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2,
                                mainAxisSpacing: 4.0,
                                crossAxisSpacing: 4.0,
                                childAspectRatio: 0.65,
                              ),
                              itemCount: snapshot.data.documents.length,
                              itemBuilder: (context, index) {
                                AtividadeData data = AtividadeData.fromDocument(
                                    snapshot.data.documents[index]);
                                data.id = this.snapshot.documentID;
                                return AtividadeTile("grid", data); // constroi um grid com as atividades presentes no snapshot
                              }
                          ),
                          ListView.builder(
                              padding: EdgeInsets.all(4.0),
                              itemCount: snapshot.data.documents.length,
                              itemBuilder: (context, index) {
                                AtividadeData data = AtividadeData.fromDocument(
                                    snapshot.data.documents[index]);
                                data.id = this.snapshot.documentID;
                                return AtividadeTile("list", data);
                              }
                          )
                        ]
                    );
                }
            )
        ),
      );
    }else{

      return Container(
        padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.view_list,
              size: 80.0, color: Theme.of(context).primaryColor,),
            SizedBox(height: 16.0,),
            Text("Faça o login para ver suas atividades!",
              style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 16.0,),
            RaisedButton(
              child: Text("Entrar", style: TextStyle(fontSize: 18.0),),
              textColor: Colors.white,
              color: Theme.of(context).primaryColor,
              onPressed: (){
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (context)=>LoginScreen())
                );
              },
            )
          ],
        ),
      );

    }
  }
}
