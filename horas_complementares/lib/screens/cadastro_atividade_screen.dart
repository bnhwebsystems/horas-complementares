import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:file_picker/file_picker.dart';
import 'package:horas_complementares/screens/home_screen.dart';
import 'package:horas_complementares/model/atividade_horas_model.dart';
import 'package:scoped_model/scoped_model.dart';

class SignAtividadeScreen extends StatefulWidget {
  @override
  _SignAtividadeScreenState createState() => _SignAtividadeScreenState();
}

class _SignAtividadeScreenState extends State<SignAtividadeScreen> {
  final _horasController = TextEditingController();
  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  File imgFile;

  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        body: ScopedModelDescendant<AtividadeModel>(
          builder: (context, child, model) {
            if (model.isLoading)
              return Center(
                child: CircularProgressIndicator(),
              );

            return Form(
              key: _formKey,
              child: Container(
                width: 331,
                height: 331,
                margin:
                    EdgeInsets.only(left: 30, top: 50, right: 30, bottom: 50),
                padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black,
                        blurRadius: 7,
                        offset: Offset(0, 3),
                      )
                    ]),
                child: Column(
                  children: <Widget>[

                    Row(
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            controller: _horasController,
                            decoration: InputDecoration(
                                hintText: "horas que você pretende abolir"),
                            validator: (text) {
                              if (text.isEmpty) return "Nome Inválido!";
                            },
                          ),
                        ),
                        SizedBox(
                          height: 16.0,
                        ),
                        IconButton(
                          icon: Icon(Icons.add),
                          onPressed: () async {
                            final FilePickerResult result = await FilePicker
                                .platform
                                .pickFiles(type: FileType.any); // precisa que mude o filtro para documentos
                            File file = File(result.files.first.path);
                            imgFile = file;
                          },
                        ),
                      ],
                    ),
                    Container(
                      alignment: Alignment.bottomCenter,
                      padding: EdgeInsets.only(bottom: 10),
                      child: RaisedButton(
                        child: Text(
                          "enviar Mensagem",
                          style: TextStyle(
                            fontFamily: "OpenSans",
                            fontSize: 18.0,
                            color: Colors.white,
                          ),
                        ),
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            Map<String, dynamic> atividadeData = {
                              "horasRequisitadas": _horasController.text,
                              "horasAbonadas": null,
                              "dataEntrega": DateTime.now().toString(),
                              "dataAvaliada": null,
                              "situacao": "nova",
                              "documentURL": null,
                            };

                            model.signAtividade(
                                atividadeData: atividadeData, imgFile: imgFile);
                          }
                        },
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ));
  }
}
