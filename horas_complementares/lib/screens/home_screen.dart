import 'package:flutter/material.dart';
import 'package:horas_complementares/tabs/atividades_tab.dart';
import 'package:horas_complementares/screens/cadastro_atividade_screen.dart';
import 'package:horas_complementares/tabs/home_tab.dart';

import 'package:horas_complementares/widgets/custom_drawer.dart';


class HomeScreen extends StatelessWidget {
  final _pageController = PageController();
  final String titulo = null;

  @override
  Widget build(BuildContext context) {
    return PageView(
      controller: _pageController,
      physics: NeverScrollableScrollPhysics(),
      children: <Widget>[

        Scaffold(
          appBar: AppBar(
            title: Text("home"),
            centerTitle: true,
            flexibleSpace: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                colors: [
                  Color.fromARGB(255, 82, 194, 52),
                  Color.fromARGB(255, 6, 23, 0)
                ],
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
              )),
            ),
          ),
          drawer: CustomDrawer(_pageController),
          body: HomeTab(),
        ),
        Scaffold(
          appBar: AppBar(
            title: Text("home"),
            centerTitle: true,
            flexibleSpace: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Color.fromARGB(255, 82, 194, 52),
                      Color.fromARGB(255, 6, 23, 0)
                    ],
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                  )),
            ),
          ),
          drawer: CustomDrawer(_pageController),
          body: SignAtividadeScreen(),
        ),
        Scaffold(
          appBar: AppBar(
            title: Text("Atividades Entregues"),
            centerTitle: true,
            flexibleSpace: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Color.fromARGB(255, 82, 194, 52),
                      Color.fromARGB(255, 6, 23, 0)
                    ],
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                  )),
            ),
          ),
          drawer: CustomDrawer(_pageController),
          body: AtividadeTab(),
        ),

        Container(color: Colors.green),
      ],
    );
  }
}
