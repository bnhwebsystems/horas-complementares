import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:horas_complementares/screens/login_screen.dart';
import 'package:scoped_model/scoped_model.dart';
import 'model/atividade_horas_model.dart';
import 'model/user_model.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return ScopedModel<UserModel>(
      model: UserModel(),
      child: ScopedModelDescendant<UserModel>(
          builder: (context, child, model){
            return ScopedModel<AtividadeModel>(
              model: AtividadeModel(model),
              child: MaterialApp(
                  title: "Horas Complementares - IFSP",
                  theme: ThemeData(
                      primarySwatch: Colors.green,
                      primaryColor: Color.fromARGB(255, 82, 194, 52)
                  ),
                  debugShowCheckedModeBanner: false,

                  home: SplashScreen(
                    title: new Text(
                      'Welcome In SplashScreen',
                      style: new TextStyle(fontWeight: FontWeight.bold,
                          fontSize: 20.0),
                    ),
                    seconds: 8,
                    navigateAfterSeconds: LoginScreen(),
                    image: new Image.asset(
                        'assets/loading.gif'),
                    backgroundColor: Colors.black,
                    styleTextUnderTheLoader: new TextStyle(),
                    photoSize: 150.0,
                    onClick: () => print("Flutter Egypt"),
                    loaderColor: Colors.white,
                  )
              ),
            );
          }
      ),
    );
  }


}
