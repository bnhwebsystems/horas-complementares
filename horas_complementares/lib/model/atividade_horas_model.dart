import 'dart:async';
import 'dart:io';
import 'package:horas_complementares/model/user_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:flutter/material.dart';

class AtividadeModel extends Model {

  UserModel user;
  Map<String, dynamic> atividadeData = Map();


  AtividadeModel(this.user) {
    if (user.isLoggedIn())
      print("logado");
  }

  bool isLoading = false;

  static AtividadeModel of(BuildContext context) =>
      ScopedModel.of<AtividadeModel>(context);

  @override
  void addListener(VoidCallback listener) {
    super.addListener(listener);
  }

  void signAtividade({@required Map<String,
      dynamic> atividadeData, @required File imgFile}) async {
    isLoading = true;
    notifyListeners();
    if (user.isLoggedIn()) { // se o usuario está logado
      StorageUploadTask task = FirebaseStorage.instance.ref().child(
          "users/atividades/" + DateTime
              .now()
              .year
              .toString() + "/" + DateTime
              .now()
              .month
              .toString() + "/" + user.firebaseUser.uid + DateTime
              .now()
              .millisecondsSinceEpoch
              .toString()).putFile(imgFile); // salva no storage no seguinte caminho essa img/doc

      StorageTaskSnapshot taskSnapshot = await task.onComplete;
      String url = await taskSnapshot.ref.getDownloadURL();
      atividadeData["documentURL"] = url; // pegar a url da img/doc que acaba de ser salva no storage

      Firestore.instance.collection("users").document(user.firebaseUser.uid)
          .collection("atividades").document(DateTime.now().year.toString())
          .collection(DateTime.now().month.toString()).document(DateTime.now().toString())
          .setData(atividadeData); // salva os dados da atividade no firestore, junto com a url para a img/doc correspondente
    }
    isLoading = false;
    notifyListeners();
  }




}
