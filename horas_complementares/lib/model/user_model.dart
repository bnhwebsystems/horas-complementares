import 'dart:async';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:flutter/material.dart';

class UserModel extends Model {

  final GoogleSignIn googleSignIn = GoogleSignIn();
  FirebaseAuth _auth = FirebaseAuth.instance;
  AuthResult authResult;
  FirebaseUser firebaseUser;
  Map<String, dynamic> userData = Map();

  bool isLoading = false;

  static UserModel of(BuildContext context) =>
      ScopedModel.of<UserModel>(context);

  @override
  void addListener(VoidCallback listener) {
    super.addListener(listener);

    _loadCurrentUser();
  }

  void signUp({@required Map<String, dynamic> userData, @required String pass,
    @required VoidCallback onSuccess, @required VoidCallback onFail, @required File imgFile}){

    isLoading = true;
    notifyListeners();

    _auth.createUserWithEmailAndPassword( //cria um usuario com email em senha
        email: userData["email"],
        password: pass
    ).then((authResult) async {
      firebaseUser = authResult.user;

      StorageUploadTask task = FirebaseStorage.instance.ref().child("users/avatars/"+firebaseUser.uid).putFile(imgFile); //upload da foto de perfil do usuario no storage

      StorageTaskSnapshot taskSnapshot = await task.onComplete;
      String url = await taskSnapshot.ref.getDownloadURL();
      userData["fotoperfil"] = url; // pegar url da foto de perfil

      await Firestore.instance.collection("users").document(firebaseUser.uid).setData(userData); // guardar as informações do usuario dentro do de um documento cujo o nome é o id unico do usuario
      notifyListeners(); // caso o banco de dados esteja vazio o comando ira criar a estrutura adequada para guardar os dados

      onSuccess();
      isLoading = false;
      notifyListeners();
    }).catchError((e){
      onFail();
      isLoading = false;
      notifyListeners();
    });

  }

  void signIn({@required String email, @required String pass,
    @required VoidCallback onSuccess, @required VoidCallback onFail}) async {

    isLoading = true;
    notifyListeners();

    _auth.signInWithEmailAndPassword(email: email, password: pass).then((authResult) async { //autentica o usuario com base em email e senha no authentication
          firebaseUser = authResult.user;


          DocumentSnapshot docUser =
          await Firestore.instance.collection("users").document(firebaseUser.uid).get();
           userData = docUser.data;
           userData['name'] = docUser['name'];
           userData['fotoperfil'] = docUser['fotoperfil']; // pega as informações do usuario, aqui podem ser adicionados campos
          notifyListeners();


          onSuccess();
          isLoading = false;
          notifyListeners();

        }).catchError((e){
      onFail();
      isLoading = false;
      notifyListeners();
    });

  }

  void signInGoogle({@required VoidCallback onSuccess, @required VoidCallback onFail}) async{ // função exclusivamente feita pra simular login com API externa
    isLoading = true;
    notifyListeners();
     GoogleSignInAccount googleSignInAccount =
    await googleSignIn.signIn();
     GoogleSignInAuthentication googleSignInAuthentication =
    await googleSignInAccount.authentication;

     AuthCredential credential = GoogleAuthProvider.getCredential(
        idToken: googleSignInAuthentication.idToken,
        accessToken: googleSignInAuthentication.accessToken);

    _auth.signInWithCredential(credential).then((authResult) async {
    firebaseUser  = authResult.user;
    await _loadCurrentUser();

    onSuccess();
    isLoading = false;
    notifyListeners();

  }).catchError((e){
  onFail();
  isLoading = false;
  notifyListeners();
  });
  }

  void signOut() async {
    await _auth.signOut();

    userData = Map();
    firebaseUser = null;

    notifyListeners();
  }

  void recoverPass(String email){
    _auth.sendPasswordResetEmail(email: email); //envia um email para recuperação de senha atraves do firebase quando acionado
  }

  bool isLoggedIn(){
    return firebaseUser != null;
  }


  Future<Null> _loadCurrentUser() async {
    if(firebaseUser == null)
      firebaseUser = await _auth.currentUser();
    if(firebaseUser != null){
       this.userData = {
      "uid": firebaseUser.uid,
      "name": firebaseUser.displayName,
      "fotoperfil": firebaseUser.photoUrl,
      "time": Timestamp.now(),
      }; // carregar as informações de perfil
    }
    notifyListeners();
  }

}