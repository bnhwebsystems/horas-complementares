import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:horas_complementares/screens/atividades_entregues_screen.dart';



class AtvdYearTile extends StatelessWidget {

  final DocumentSnapshot snapshot;

  AtvdYearTile(this.snapshot);



  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading:Text(snapshot.documentID) ,
      trailing: Icon(Icons.keyboard_arrow_right),
      onTap: (){
        Navigator.of(context).push(
            MaterialPageRoute(builder: (context)=> AtividadeEntregueScreen(snapshot))
        );
      },
    );
  }
}
