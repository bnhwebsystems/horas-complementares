import 'package:flutter/material.dart';
import 'package:horas_complementares/datas/atividade_data.dart';
import 'package:horas_complementares/screens/atividades_entregues_screen.dart';

class AtividadeTile extends StatelessWidget {
  final String type;
  final AtividadeData atividade;

  AtividadeTile(this.type, this.atividade);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){

      },
      child: Card(
        child: type == "grid"
            ? Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            AspectRatio(
              aspectRatio: 0.8,
              child: Image.network(
                atividade.documentURL,
                fit: BoxFit.cover,
              ),
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    Text(
                      atividade.dataEntrega,
                      style: TextStyle(fontWeight: FontWeight.w500),
                    ),
                  ],
                ),
              ),
            )
          ],
        )
            : Row(
          children: <Widget>[
            Flexible(
              flex: 1,
              child: Image.network(
                atividade.documentURL, //precisa desenvolver um modo de documentos serem exibidos ao ocorrer um click
                fit: BoxFit.cover,
                height: 250.0,
              ),
            ),
            Flexible(
              flex: 1,
              child: Container(
                padding: EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      atividade.dataEntrega,
                      style: TextStyle(fontWeight: FontWeight.w500),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
