import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';


class AtvdMonthTile extends StatelessWidget {

  final DocumentSnapshot snapshot;

  AtvdMonthTile(this.snapshot);



  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading:Text(snapshot.documentID) ,
      trailing: Icon(Icons.keyboard_arrow_right),
      onTap: (){

      },
    );
  }
}
