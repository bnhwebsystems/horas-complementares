import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:horas_complementares/model/user_model.dart';
import 'package:horas_complementares/screens/login_screen.dart';
import 'package:horas_complementares/tiles/atvd_year_tile.dart';


class AtividadeTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    if(UserModel.of(context).isLoggedIn()) { //checa se está logado
      String uid = UserModel
          .of(context)
          .firebaseUser
          .uid;

      return Scaffold(
      body: FutureBuilder<QuerySnapshot>(
        future: Firestore.instance.collection("users").document(uid).collection("atividades").getDocuments(), // tira uma snapshot dos anos que possuem atividades no banco
        builder: (context, snapshot) {
          if (!snapshot.hasData)
            return Center(
              child: CircularProgressIndicator(),
            );
          else {
            var dividedTiles = ListTile
                .divideTiles(
                tiles: snapshot.data.documents.map((doc) {
                  return AtvdYearTile(doc);
                }).toList(),
                color: Colors.grey[500])
                .toList();

            return ListView(
              children: dividedTiles,
            );
          }
        },
      ),
      );
    }else{

      return Container(
        padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.view_list,
              size: 80.0, color: Theme.of(context).primaryColor,),
            SizedBox(height: 16.0,),
            Text("Faça o login para ver suas atividades!",
              style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 16.0,),
            RaisedButton(
              child: Text("Entrar", style: TextStyle(fontSize: 18.0),),
              textColor: Colors.white,
              color: Theme.of(context).primaryColor,
              onPressed: (){
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (context)=>LoginScreen())
                );
              },
            )
          ],
        ),
      );

    }
    }
  }
