import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget {

  final String titulo;

  CustomAppBar(this.titulo);


  @override
  Widget build(BuildContext context) {
    return Material(
        child: AppBar(
        title: Text("Login"),
    centerTitle: true,
    flexibleSpace: Container(
    decoration: BoxDecoration(
    gradient: LinearGradient(
    colors: [
    Color.fromARGB(255, 82, 194, 52),
    Color.fromARGB(255, 6, 23, 0)
    ],
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
    )),
    ),
    ),
    );}
}
