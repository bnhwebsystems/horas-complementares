import 'package:flutter/material.dart';
import 'package:horas_complementares/model/user_model.dart';
import 'package:horas_complementares/screens/login_screen.dart';
import 'package:horas_complementares/tiles/drawer_tile.dart';
import 'package:scoped_model/scoped_model.dart';

class CustomDrawer extends StatelessWidget {

  final PageController pageController;

  CustomDrawer(this.pageController);

  @override
  Widget build(BuildContext context) {
    Widget _buildDrawerBack() =>
        Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [
                    Color.fromARGB(255, 6, 23, 0),
                    Color.fromARGB(255, 82, 194, 52),

                  ],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
              )
          ),
        );

    return Drawer(
      child: Stack(
        children: <Widget>[
          _buildDrawerBack(),
          ListView(
            padding: EdgeInsets.only(left: 32.0, top: 16.0),
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(bottom: 8.0),
                padding: EdgeInsets.fromLTRB(0.0, 16.0, 16.0, 8.0),
                height: 170.0,
                child: Stack(
                  children: <Widget>[
                    Positioned(
                        top: 8.0,
                        left: 0.0,
                        bottom: 0.0,
                        child: ScopedModelDescendant<UserModel>(
                          builder: (context, child, model) {
                            return Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  CircleAvatar(
                                    backgroundImage: NetworkImage(
                                       "${!model.isLoggedIn() ? "" : model.userData["fotoperfil"]}"),
                                  ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                            Text("Olá, ${!model.isLoggedIn() ? "" : model
                                .userData["name"]}",
                            style: TextStyle(
                            fontSize: 18.0,
                            fontWeight: FontWeight.bold
                            ),
                            ),
                            GestureDetector(
                            child: Text(
                            !model.isLoggedIn() ?
                            "Entre ou cadastre-se >"
                                : "Sair",
                            style: TextStyle(
                            color: Theme
                                .of(context)
                                .primaryColor,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold
                            ),
                            ),
                            onTap: () {
                            if (!model.isLoggedIn())
                            Navigator.of(context).push(
                            MaterialPageRoute(
                            builder: (context) =>
                            LoginScreen())
                            );
                            else
                            model.signOut();
                            },
                            )
                            ],
                            ),
                            ],
                            );
                            }
                        )
                    )
                  ],
                ),
              ),
              Divider(),
              DrawerTile(Icons.person, "PERFIL", pageController, 0),
              DrawerTile(Icons.list, "ENTREGAR ATIVIDADE", pageController, 1),
              DrawerTile(Icons.playlist_add_check, "ATIVIDADES ENTREGUES", pageController, 2),
              DrawerTile( Icons.playlist_add_check, "ATIVIDADES ENTREGUES", pageController, 3),
              DrawerTile( Icons.playlist_add_check, "OPÇÕES", pageController, 4),
            ],
          )
        ],
      ),
    );
  }
}