import 'package:cloud_firestore/cloud_firestore.dart';

class AtividadeData{
  String id; //defição dos atributos da AtividadeData
  String dataAvaliada;
  String dataEntrega;
  String documentURL;
  String horasAbonadas;
  String horasRequisitadas;
  String situacao;

  AtividadeData.fromDocument(DocumentSnapshot snapshot){ //recebe um DocumentSnapshot, que seria como uma foto do estado atual do banco de dados
    id = snapshot.documentID;
    dataAvaliada = snapshot.data["dataAvaliada"]; //preenche os atributos da classe com os dados da snapshot
    dataEntrega = snapshot.data["dataEntrega"];
    documentURL = snapshot.data["documentURL"];
    horasAbonadas = snapshot.data["horasAbonadas"];
    horasRequisitadas = snapshot.data["horasRequisitadas"];
    situacao =  snapshot.data["situacao"];
  }

  Map<String, dynamic> toResumedMap(){
    return {
      "id" : id,      // um map string resumido para carregamento rápido
      "documentURL" : documentURL,
      "situacao" : situacao
    };
    }
  }
