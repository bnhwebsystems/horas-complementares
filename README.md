# App de Horas Complementares #

AUTORES:
	ESTHER LEÃO
	FABIO ALVES
	JOAQUIM BENONI EUZÉBIO

## Tecnologias usadas ##
- Dart
- Flutter
-Firebase
	- Cloud Firestrore
	- Authentication
	- Storage

## O que foi feito até agora ##
Dentro do app, ja conseguimos logar utilizando o firebase.
Temos uma pagina de cadastro de usuarios com a opção de subir sua imagem de perfil.
O aluno ja consegue subir o arquivo para pedir as horas complementares
com o upload do arquivo o sistemna armazena no firestore diversas informações sobre a atividade, como data de entrega
O aluno pode visualizar um préview das atividades entregues

##O que precisa para continuar o desenvolvimento do app ##
Criar um novo banco no firebase e configura-lo, existem varios tutoriais de como fazer, criar um arquivo google service e etc.

## IDE Utilizada ##
- Android Studio

## Prótotipo XD
Foi feito um prototipo de design para o projeto utilizando adobe xd.
O arquivo se encontra no repositorio.



### Dicas ###
Se você está familiarizado com Java, irá entender rapidamente como o Dart Funciona,
sugiro fazer cursos sobre a criação de apps mobiles com Dart e Flutter, mas o código possui diversis comentarios explicando modelo de funções